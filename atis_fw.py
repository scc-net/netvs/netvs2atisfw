#!/usr/bin/env python3

from netdb_client import APIEndpoint, APISession
import netdb_client.util
import json
import re
import os

parser = netdb_client.util.ArgumentParser(description='Get ATIS firewall rules')
parser.add_argument('subnets', help='Comma-separated list of subnets')
parser.add_argument('-o', '--outdir', help='Output directory', default='.')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

# KIT-Netze von hier https://www.scc.kit.edu/dienste/ip-subnets.php
valid_gebiet = ['informatik', 'kit', 'worldwide']
src_v4nets_by_gebiet = {'informatik': [ '141.3.0.0/17' ], 'kit': ['129.13.0.0/16','141.3.0.0/16','141.52.0.0/16','185.237.152.0/22','193.196.32.0/20','172.16.0.0/12','192.168.0.0/16' ], 'worldwide': [ '0.0.0.0/0' ]}
src_v6nets_by_gebiet = {'informatik': [ '2a00:1398:2::/48' ], 'kit': [ '2a00:1398::/32','2a00:139e::/32' ], 'worldwide': [ '::/0' ]}

ta = list()

subnets = [s.strip() for s in args.subnets.split(',')]
for s in subnets:
    ta.append({'idx': s + '_list', 'name': 'nd.ip_subnet.list', 'old': {'cidr': s, 'cidr_operator': 'ctdby'}})
    ta.append({'idx': s + '_addr', 'name': 'dns.ip_addr.list', 'join': {s + '_list': 'default'}})
    ta.append({'idx': s + '_addr_records', 'name': 'dns.record.list', 'old': {'target_is_reverse_unique': True},
               'join': {s + '_addr': 'default'}})

res = api.execute_ta(ta, dict_mode=True)
fw_rules = []
records_by_fqdn = dict()
for s in subnets:
    fqdn_recs = dict(netdb_client.util.list_to_generator_map_one2many(res[s + '_addr_records'], 'fqdn'))
    for f, r in fqdn_recs.items():
        if f not in records_by_fqdn:
            records_by_fqdn[f] = r
        else:
            records_by_fqdn[f].extend(r)
for s in subnets:
    for r in records_by_fqdn.values():
        rec4 = None
        rec6 = None
        for rec in r:
            assert len(rec['target_bcd_list']) == 1
            if rec['type'] == 'AAAA':
                rec6 = rec
            elif rec['type'] == 'A':
                rec4 = rec
            else:
                raise ValueError(f"Unhandled record type '{rec['type']}'")
        tcp_ports = list()
        gebiet = None
        if r[0]['fqdn_description'] is not None:
            rules = re.search(r"(?im)(atis:([a-z]+):([a-z]+))", r[0]['fqdn_description'])
            if rules is not None and len(rules.groups()) == 3:
                if rules.group(3).lower() in valid_gebiet:
                    gebiet = rules.group(3).lower()
                    if rules.group(2).lower() == 'web':
                        tcp_ports.append(80)
                        tcp_ports.append(443)
                    if gebiet in valid_gebiet: 
                        fw_rules.append(
                            {
                                'IPv4': rec4['data'] if rec4 is not None else None,
                                'IPv6': rec6['data'] if rec6 is not None else None,
                                'Ports': {'udp': {}, 'tcp': {'accept': tcp_ports}, 'comments': [r[0]['fqdn_description']]},
                                'gebiet': gebiet
                            })

# Transform into new format as postulated in mail from 23.02.23, 14:41
for r in fw_rules:
    if r['IPv4'] is not None:
        with open(os.path.join(args.outdir, r['IPv4'].replace('.', '-')), 'w') as f:
            obj = {'ports': {}}
            r['Ports']['source'] = {'net': src_v4nets_by_gebiet[r['gebiet']]}
            obj['ports'][r['IPv4']] = r['Ports']
            json.dump(obj, f, indent=4)
    if r['IPv6'] is not None:
        with open(os.path.join(args.outdir, r['IPv6'].replace(':', '_')), 'w') as f:
            obj = {'ports': {}}
            r['Ports']['source'] = {'net': src_v6nets_by_gebiet[r['gebiet']]}
            obj['ports'][r['IPv6']] = r['Ports']
            json.dump(obj, f, indent=4)
